import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-portail',
  standalone: true,
  imports: [CommonModule, MatButtonModule],
  templateUrl: './portail.component.html',
  styleUrls: ['./portail.component.scss']
})
export class PortailComponent {
  isTommyHere: boolean = false;
}
