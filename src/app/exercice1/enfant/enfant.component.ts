import { Component } from '@angular/core';

@Component({
  selector: 'app-enfant',
  standalone: true,
  templateUrl: './enfant.component.html',
  styleUrls: ['./enfant.component.scss']
})
export class EnfantComponent {
  message: string = '?';
}
