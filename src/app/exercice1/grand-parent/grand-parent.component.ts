import { Component } from '@angular/core';
import { ParentComponent } from '../parent/parent.component';
import {MatIconModule} from '@angular/material/icon';

@Component({
  selector: 'app-grand-parent',
  standalone: true,
  imports: [ParentComponent, MatIconModule],
  templateUrl: './grand-parent.component.html',
  styleUrls: ['./grand-parent.component.scss']
})
export class GrandParentComponent {
  message: string = 'Bonjour !';
}
