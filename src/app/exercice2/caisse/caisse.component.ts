import { Component } from '@angular/core';
import { ChariotComponent } from '../chariot/chariot.component';
import { MatButtonModule } from '@angular/material/button';

@Component({
  selector: 'app-caisse',
  standalone: true,
  imports: [ChariotComponent, MatButtonModule],
  templateUrl: './caisse.component.html',
  styleUrls: ['./caisse.component.scss']
})
export class CaisseComponent {
  count: number = 0;
}
