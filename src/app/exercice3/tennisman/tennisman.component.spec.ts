import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TennismanComponent } from './tennisman.component';

describe('TennismanComponent', () => {
  let component: TennismanComponent;
  let fixture: ComponentFixture<TennismanComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [TennismanComponent]
    });
    fixture = TestBed.createComponent(TennismanComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
