import { Component } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSelectModule } from '@angular/material/select';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-exercice4',
  standalone: true,
  imports: [MatButtonModule, MatFormFieldModule, MatSelectModule, FormsModule, CommonModule],
  templateUrl: './exercice4.component.html',
  styleUrls: ['./exercice4.component.scss']
})
export class Exercice4Component {
  // 1. ngIf
  valeur: boolean = false;

  // 2. ngFor
  nombres: { id: number, nombre: number}[] = [
    { id: 1, nombre: 0 },
    { id: 2, nombre: 1 },
    { id: 3, nombre: 5 },
    { id: 4, nombre: 58 },
    { id: 5, nombre: 1 },
    { id: 6, nombre: 963 },
    { id: 7, nombre: 2 },
    { id: 8, nombre: 1 },
    { id: 9, nombre: 5 },
    { id: 10, nombre: 156 },
  ];
  
  // 3. ngSwitch
  couleur: 'rouge' | 'vert' | 'bleu' = 'rouge';

  // 4. Property binding des éléments du DOM - Class
  // 5. Property binding des éléments du DOM - Style
  condition: boolean = true;

  // 6. Two-way binding : ngModel
  email: string = '';

  changerCouleur(): void {
    // TODO : question 3
  }
}
