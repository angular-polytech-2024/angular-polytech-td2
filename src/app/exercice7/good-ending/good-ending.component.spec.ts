import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GoodEndingComponent } from './good-ending.component';

describe('GoodEndingComponent', () => {
  let component: GoodEndingComponent;
  let fixture: ComponentFixture<GoodEndingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [GoodEndingComponent]
    });
    fixture = TestBed.createComponent(GoodEndingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
