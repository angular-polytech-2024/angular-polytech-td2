import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BadEndingComponent } from './bad-ending.component';

describe('BadEndingComponent', () => {
  let component: BadEndingComponent;
  let fixture: ComponentFixture<BadEndingComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [BadEndingComponent]
    });
    fixture = TestBed.createComponent(BadEndingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
